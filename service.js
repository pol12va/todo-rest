"use strict"

let MongoClient = require('mongodb').MongoClient,
    service = {
        get: get,
        getAll: getAll
    };

const URL = "mongodb://localhost:27017",
    DATABASE_NAME = "TODO";

function get(id, callback) {
    MongoClient.connect(URL + "/" + DATABASE_NAME, function(err, db) {
        if (err) {
            console.log(err);
            return;
        }

        db.collection('todos').findOne({ _id: id }, function(err, todo) {
            if (err) {
                console.log(err);
                return;
            }
            if (!err) {
                callback(err, todo);
            }
            db.close();
        });
    });
}

function getAll(callback) {
    MongoClient.connect(URL + "/" + DATABASE_NAME, function(err, db) {
        if (err) {
            console.log(err);
            return;
        }

        db.collection('todos').find().toArray(function(err, todos) {
            if (!err) {
                callback(todos);
            }
            db.close();
        });
    });
}

function create(todo, callback) {
    MongoClient.connect(URL + "/" + DATABASE_NAME, function(err, db) {
        if (err) {
            console.log(err);
            return;
        }

        db.collection('todos').insert({ title: todo.title,
            completed: todo.completed, estimation: todo.estimation },
            function(err, result) {
                callback(err);
            });
    });
}

module.exports = service;