"use strict"
class Todo {
    constructor(title, completed, estimation) {
        this.title = title;
        this.completed = completed;
        this.estimation = estimation;
    }
}

module.exports = Todo;