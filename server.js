"use strict"

let express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    port,
    router,
    todoService = require('./service.js'),
    Todo = require('./Todo.js');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

port = process.env.PORT || 8080;

router = express.Router();

router.get('/', function(req, res) {
    res.json({ message: 'It\'s todo service' });
});

router.get('/todos/:todo_id', function(req, res) {
    todoService.get(Number(req.params.todo_id), function(err, todo) {
        if (!err) {
            res.json(todo);
        }
    });
});

router.get('/todos', function(req, res) {
    todoService.getAll(function(todos) {
        res.json(todos);
    });
});

router.post('/todos', function(req, res) {
    let todo = new Todo(req.body.title, req.body.completed, req.body.estimation);

    todoService.create(todo, function(err) {
        if (!err) {
            res.json('Todo created');
        }
    });
});

app.use('/todo', router);

app.listen(port);
console.log('Server started on port ' + port);